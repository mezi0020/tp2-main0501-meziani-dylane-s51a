using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class declencheur : MonoBehaviour
{
    public int cubeCount = 0;
    public TMP_Text cubeCounterText;

    private void Start()
    {
        UpdateCounterUI(); // Mettre à jour le compteur au démarrage
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Charge"))
        {
            cubeCount++;
            UpdateCounterUI();
            Debug.Log("Cube ajouté. Total: " + cubeCount);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Charge"))
        {
            UpdateCounterUI();
            Debug.Log("Cube retiré. Total: " + cubeCount);
        }
    }

    // Mettre à jour le texte du compteur dans l'UI
    void UpdateCounterUI()
    {
        if (cubeCounterText != null)
        {
            cubeCounterText.text = "Compteur : " + cubeCount; // Mettre à jour le texte
        }
    }
}
