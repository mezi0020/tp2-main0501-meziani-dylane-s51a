using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Camera cameraCabine;
    public Camera cameraTopView;
    public Camera cameraRearView;

    // Start is called before the first frame update
    void Start()
    {
        // Activer une caméra par défaut, désactiver les autres
        ActivateCamera(cameraTopView);
    }

    // Update is called once per frame
    void Update()
    {
        // Basculer entre les caméras avec les touches du clavier : 1(&), 2(é) et 3(")
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            ActivateCamera(cameraCabine);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            ActivateCamera(cameraTopView);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            ActivateCamera(cameraRearView);
        }
    }

    void ActivateCamera(Camera activeCamera)
    {
        // Désactiver toutes les caméras
        cameraCabine.gameObject.SetActive(false);
        cameraTopView.gameObject.SetActive(false);
        cameraRearView.gameObject.SetActive(false);

        // Activer uniquement la caméra sélectionnée
        activeCamera.gameObject.SetActive(true);
    }
}
